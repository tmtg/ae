sel_pat_0 = uigetdir;      % Select the pictures folder path
addpath(sel_pat_0);
fol_lis =  dir(fullfile(sel_pat_0,'*.MOV')); 
fileName = fol_lis.name; 
obj = VideoReader(fileName); 
numFrames = obj.NumberOfFrames; 
for k = 1 : numFrames 
     frame = read(obj,k); 
     imwrite(frame,strcat(sel_pat_0,'\',num2str(k),'.jpg'),'jpg'); 
end
addpath(sel_pat_0);
img_folder = dir(fullfile(sel_pat_0,'*.jpg'));
img_num = length(img_folder);
Y_value = cell(img_num,1);

img_capture = img_folder(1).name;
rect_image = imread(img_capture);
[image2,rect] = imcrop(rect_image);

if img_num > 0
    for i = 1:img_num
        image_name = img_folder(i).name; 
        image = imread(image_name);
        im2 = imcrop(image,rect); % Crops the image,
        % Rect is a four-element position vector that specifies the size and position of the crop rectangle.
        r = round(mean(mean(im2(:,:,1))));
        g = round(mean(mean(im2(:,:,2))));
        b = round(mean(mean(im2(:,:,3))));
        rgb = [r,g,b];
        Y = 0.299*r + 0.587*g + 0.114*b;
        
        mean_Y = mean(mean(Y));
        Y_data = {mean_Y};
        Y_value(i,:) = Y_data;
        
    end
end
b = 1:img_num;
excel_data = cell(img_num,2);
excel_data(:,1) = num2cell(b);
excel_data(:,2) = Y_value;
excel_title = {'Frames','Y Value'};
excel = [excel_title;excel_data];
xlswrite(strcat(sel_pat_0,'\','AE Speed.xlsx'),excel,1);

%%
filespec_user=strcat(sel_pat_0,'\','AE Speed.xlsx');
excel = actxserver('Excel.Application');
excel.visible = 1;
workbooks = excel.Workbooks;
workbook = workbooks.Open (filespec_user);

chart = excel.ActiveSheet.Shapes.AddChart();
chart.Name = 'AE Performance';
ExpChart = excel.ActiveSheet.ChartObjects('AE Performance');
ExpChart.Activate;

excel.ActiveChart.ChartType = 'xlXYScatterLines'; 
excel.ActiveChart.HasTitle = 1;
excel.ActiveChart.ChartTitle.Characters.Text = 'AE Performance';
 
for i_delete = 1:2
  try
    Series = invoke(excel.ActiveChart,'SeriesCollection',1);
    invoke(Series,'Delete');
  catch e
  end
end
NewSeries = invoke(excel.ActiveChart.SeriesCollection,'NewSeries');
NewSeries.XValues = ['Sheet1' '!A' int2str(1) ':A' int2str(img_num+1)];
NewSeries.Values  = ['Sheet1' '!B' int2str(1) ':B' int2str(img_num+1)];
NewSeries.Name    = 'Y Value';

% Setting the (X-Axis) and (Y-Axis) titles. 
ChartAxes = chart.Chart.Axes(1); 
set(ChartAxes,'HasTitle',1); 
set(ChartAxes.AxisTitle,'Caption','Frames'); 
ChartAxes = chart.Chart.Axes(2);  
set(ChartAxes,'HasTitle',1); 
set(ChartAxes.AxisTitle,'Caption','Value'); 

% Setting the chart size    
excel.ActiveChart.ChartArea.Width = 1500;
excel.ActiveChart.ChartArea.Height = 400;

% Placement
GetPlacement = get(excel.ActiveSheet,'Range', 'C1');
ExpChart.Left = GetPlacement.Left;
ExpChart.Top = GetPlacement.Top;

workbook.Save();
workbook.Close();
excel.Quit();
disp('Done!');