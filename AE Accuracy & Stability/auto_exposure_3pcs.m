clear;clc;
folder_path = uigetdir;      % Select the pictures folder path
addpath(folder_path);

img_path_list =  dir(fullfile(folder_path,'*.jpg'));       % Find the *.raw file
img_num = length(img_path_list);               % Count how many '*.jpg' files

value_results = cell(img_num,1);
file_name = cell(img_num,1);
exp_time = zeros(img_num,1);
iso = zeros(img_num,1);

if img_num > 0                                                % If the folder has files
    for i = 1:img_num                                         % Read image one-by-one
            image_name = img_path_list(i).name; 
            file_name(i) = {image_name};
            img_inf = imfinfo(cell2mat(file_name(i)));  
            exp_time(i) = img_inf.DigitalCamera.ExposureTime;
            iso(i) = img_inf.DigitalCamera.ISOSpeedRatings;
            
            image = imread(image_name);
            [h,w,~] = size(image);            
            r = image(0.25*h:0.75*h,0.25*w:0.75*w,1);
            g = image(0.25*h:0.75*h,0.25*w:0.75*w,2);
            b = image(0.25*h:0.75*h,0.25*w:0.75*w,3);
            Y = 0.299*r + 0.587*g + 0.114*b;
            
            mean_Y = mean(mean(Y));
            
            mean_results_write = {mean_Y};
            value_results(i,:) = mean_results_write;

    end    
end
    ev = (4:14)';
    cd1 = [2.13;4.26;8.56;17.14;34.25;68.60;137.5;274.4;546.2;1087;2176];
    cyc_num = img_num/11;
    lux1 = roundn(cd1.*pi,-2);
    lux2 = lux1(end:-1:1);

    ev0 = repmat(ev,cyc_num,1);
    cdm2 = repmat(lux1,cyc_num,1);
    
    excel_data = cell(img_num,6);
    for n = 1:cyc_num
        start = 1+(n-1)*11;
        end_num = start+10;
        for j = start:end_num
            excel_data(j,1) = {ev0(j)};
            excel_data(j,2) = {cdm2(j)};
            excel_data(j,3) = file_name(j);
            excel_data(j,4) = {iso(j)};
            excel_data(j,5) = {exp_time(j)};
            excel_data(j,6) = value_results(j);
        end
    end
    excel_data_1 = excel_data(1:11,:);
    excel_data_2 = excel_data(12:22,:);
    excel_data_3 = excel_data(23:33,:);
    excel_whole = [excel_data_1,excel_data_2,excel_data_3];
    excel_title = {'Lv','Lux','Name','ISO','EXP Time','Y Value'};
    excel_title = repmat(excel_title,1,cyc_num);
    excel = [excel_title;excel_whole];
    
    xlswrite(strcat(folder_path,'\','Y channel_results.xlsx'),excel,1);

    filespec_user=strcat(folder_path,'\','\Y channel_results.xlsx');
    excel = actxserver('Excel.Application');
    excel.visible = 1;
    workbooks = excel.Workbooks;
    workbook = workbooks.Open (filespec_user);
    sheets = workbook.Sheets;
    
    chart = excel.ActiveSheet.Shapes.AddChart();
    chart.Name = 'Y Channel Mean Value';
    ExpChart = excel.ActiveSheet.ChartObjects('Y Channel Mean Value');
    ExpChart.Activate;
    
    excel.ActiveChart.ChartType = 'xlXYScatterLines'; 
    excel.ActiveChart.HasTitle = 1;
    excel.ActiveChart.ChartTitle.Characters.Text = 'Y Channel Mean Value';
    
    for i_delete = 1:cyc_num*6
      try
        Series = invoke(excel.ActiveChart,'SeriesCollection',1);
        invoke(Series,'Delete');
      catch e
      end
    end
    
    NewSeries = invoke(excel.ActiveChart.SeriesCollection,'NewSeries');
    NewSeries.XValues = ['Sheet1' '!A' int2str(2) ':A' int2str(img_num/cyc_num+1)];
    NewSeries.Values  = ['Sheet1' '!F' int2str(2) ':F' int2str(img_num/cyc_num+1)];
    NewSeries.Name    = 'Y Value_first';   
    
    NewSeries = invoke(excel.ActiveChart.SeriesCollection,'NewSeries');
    NewSeries.XValues = ['Sheet1' '!G' int2str(2) ':G' int2str(img_num/cyc_num++1)];
    NewSeries.Values  = ['Sheet1' '!L' int2str(2) ':L' int2str(img_num/cyc_num++1)];
    NewSeries.Name    = 'Y Value_second';
    
    NewSeries = invoke(excel.ActiveChart.SeriesCollection,'NewSeries');
    NewSeries.XValues = ['Sheet1' '!M' int2str(2) ':M' int2str(img_num/cyc_num++1)];
    NewSeries.Values  = ['Sheet1' '!R' int2str(2) ':R' int2str(img_num/cyc_num++1)];
    NewSeries.Name    = 'Y Value_third';   
    
    % Setting the (X-Axis) and (Y-Axis) titles. 
    ChartAxes = chart.Chart.Axes(1); 
    set(ChartAxes,'HasTitle',1); 
    set(ChartAxes.AxisTitle,'Caption','Lv.'); 
    ChartAxes = chart.Chart.Axes(2);  
    set(ChartAxes,'HasTitle',1); 
    set(ChartAxes.AxisTitle,'Caption','Y Channel Value'); 
    
    % Setting the (Axis) Scale 
    excel.ActiveChart.Axes(1).Select; 
    excel.ActiveChart.Axes(1).MinimumScale = 4;             
    excel.ActiveChart.Axes(1).MaximumScale = 16;
    excel.ActiveChart.Axes(1).MajorUnit = 1;
    
    excel.ActiveChart.Axes(2).Select; 
    excel.ActiveChart.Axes(2).MinimumScale = 100;             
    excel.ActiveChart.Axes(2).MaximumScale = 230;
    
    % Setting the chart size    
    excel.ActiveChart.ChartArea.Width = 450;
    
    % Placement
    GetPlacement = get(excel.ActiveSheet,'Range', 'A13');
    ExpChart.Left = GetPlacement.Left;
    ExpChart.Top = GetPlacement.Top;
    
    workbook.Save();
    workbook.Close();
    excel.Quit();
    disp('Done!');
%end